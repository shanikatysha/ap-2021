package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    public String getType() {
        return "Impenetrable Barrier";
    }

    public String defend() {
        return "building... barrier ready to use";
    }
    //ToDo: Complete me
}
