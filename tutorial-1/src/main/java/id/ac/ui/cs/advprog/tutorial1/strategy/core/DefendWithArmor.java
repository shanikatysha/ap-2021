package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    public String getType() {
        return "Mighty Armor";
    }

    public String defend() {
        return "polishing... armor ready to use";
    }
    //ToDo: Complete me
}
