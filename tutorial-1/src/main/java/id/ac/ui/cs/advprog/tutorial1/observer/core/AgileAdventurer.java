package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        //delivery rumble
        //ToDo: Complete Me
    }

    public void update() {
        if (guild.getQuestType().equalsIgnoreCase("D") || guild.getQuestType().equalsIgnoreCase("R")) {
            add(guild.getQuest());
        }
    }

    //ToDo: Complete Me
}
