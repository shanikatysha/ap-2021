package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    public String getType() {
        return "Magic";
    }

    public String attack() {
        return "attack now! abracadabra";
    }
    //ToDo: Complete me
}
